//
//  OfferDetailCellCollectionViewCell.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 18/12/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class HomeOffersDetailCell: UICollectionViewCell {

    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDiscount:UILabel!
    
    @IBOutlet weak var imgLock:UIImageView!
    @IBOutlet weak var shadowView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
