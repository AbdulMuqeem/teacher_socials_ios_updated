//
//  AlertView.swift
//  Food
//
//  Created by Abdul Muqeem on 12/10/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

class AlertView: UIView {
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var imgAlert:UIImageView!
    
    @IBOutlet weak var btnOkAction:UIButton!
    @IBOutlet weak var btnCancel:UIButton!
    @IBOutlet weak var btnOk:UIButton! // Single
    
    @IBOutlet weak var imgAlertHeight:BaseLayoutConstraint!
    @IBOutlet weak var viewHeightConstraints:BaseLayoutConstraint!
    
    var view: UIView!
    var delegate:AlertViewDelegate?
    var delegateAction:AlertViewDelegateAction?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    
    func xibSetup(){
        
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
    }
    
    
    func loadViewFromNib() -> UIView{
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
        
    }
    
    
    @IBAction func okAction(_ sender : UIButton) {
        
        self.delegate?.okAction()
        
    }
    
    @IBAction func okButtonAction(_ sender : UIButton) {
        
        self.delegateAction?.okButtonAction()
        
    }
    
    @IBAction func cancelAction(_ sender : UIButton) {
        
        self.delegateAction?.cancelAction()
        
    }
    
    func alertShow(image:UIImage , title:String , msg:String , id:Int) {
        if id == 1 {
            
            self.btnOkAction.isHidden = false
            self.btnCancel.isHidden = false
            self.btnOk.isHidden = true
            
        }
        else if id == 3
        {
            self.btnOkAction.isHidden = true
            self.btnCancel.isHidden = true
            self.btnOk.isHidden = false
            self.btnOk.setTitle("Done", for: .normal)
            
        }
        else {
            
            self.btnOkAction.isHidden = true
            self.btnCancel.isHidden = true
            self.btnOk.isHidden = false
            
        }
        self.imgAlert.image = image
        self.lblTitle.text = title
        self.lblMessage.text = msg
    }
    
}
