//
//  UIColorExtension.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(rgb: UInt) {
        
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func randomFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    
    static func random() -> UIColor {
        return UIColor(red:   UIColor.randomFloat(),
                       green: UIColor.randomFloat(),
                       blue:  UIColor.randomFloat(),
                       alpha: 1.0)
    }
    
    convenience init(colorCode: String, alpha: CGFloat = 1) {
        self.init(hex: colorCode, alpha: alpha)
    }
    
    convenience init(hex: String, alpha: CGFloat = 1) {
        var string = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if string.hasPrefix("#") {
            string.remove(at: string.startIndex)
        }
        
        guard string.count == 6 else {
            assertionFailure("Hex string should have 6 characters (besides the #)")
            self.init(colorCode: "#ffffff", alpha: alpha)
            return
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: string).scanHexInt32(&rgbValue)
        
        let red = (rgbValue >> 16) & 0xff
        let green = (rgbValue >> 8) & 0xff
        let blue = rgbValue & 0xff
        
        self.init(r: CGFloat(red), g: CGFloat(green), b: CGFloat(blue), alpha: alpha)
    }
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1) {
        assert(r >= 0 && r <= 255, "Invalid red")
        assert(g >= 0 && g <= 255, "Invalid green")
        assert(b >= 0 && b <= 255, "Invalid blue")
        
        self.init(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
    
    convenience init(oneCode: CGFloat, alpha: CGFloat = 1) {
        self.init(r: oneCode, g: oneCode, b: oneCode, alpha: alpha)
    }
}


extension UIView
{
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor)
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
