//
//  SocialManager.swift
//  Syllabit
//
//  Created by Protege Global on 20/03/2018.
//  Copyright © 2018 Protege Global. All rights reserved.
//

/*
 AppDelegate
 
 Include these lines in didFinishLaunchingWithOptions function in AppDelegate
 
 SocialManager.GoogleAuth(clientID: "446265902599-v5bt4ltp2765052ba2sjvrchkrpo3v72.apps.googleusercontent.com")
 SocialManager.TwitterAuth(consumerKey: "olaXZZaHr3qF7ETwZTg0TazML", consumerSecret: "s7OOLKLe1Tm5RlFYH7U5LkqhC1n7tXx32Ou72MfqgI0UGLbeJ2")
 SocialManager.EnableFacebook(application: application, launchOptions: launchOptions)
 */

import Foundation
import GoogleSignIn
import FBSDKLoginKit

class SocialManager: NSObject {
    
    //MARK: URL Scheme
    static func URLScheme(app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if(url.scheme!.isEqual("fbauth2:/")) {
            print("Facebook url scheme")
            
            let handled: Bool = ApplicationDelegate.shared.application(app, open: url, sourceApplication: (options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String), annotation: options[UIApplication.OpenURLOptionsKey.annotation])
            return handled
        }
        else {
            print("Google+ url scheme")
            
            let handled: Bool = GIDSignIn.sharedInstance().handle(url)
            
            return handled
        }
    }
    
    //MARK: Google
    static func GoogleAuth(clientID: String) {
        GIDSignIn.sharedInstance().clientID = clientID
    }
    
    static func GoogleSetDelegate(delegate : GIDSignInDelegate) {
        GIDSignIn.sharedInstance().delegate = delegate
        print("Social Manager - Google - Delegate: Set Successfully")
    }
    
    static func GoogleLogin() {
        if GIDSignIn.sharedInstance().delegate == nil {
            print("Social Manager - Google - Delegate: Error")
            return
        }
        GIDSignIn.sharedInstance().signIn()
    }
    
    static func GoogleIsUserLogedIn() -> Bool {
        if SocialManager.GoogleGetUser() != nil {
            print("Social Manager - Google - User: Signed In")
            return true
        }
        else {
            print("Social Manager - Google - User: Not Signed In")
            return false
        }
    }
    
    static func GoogleGetSharedInstance() -> GIDSignIn? {
        return GIDSignIn.sharedInstance()
    }
    
    static func GoogleGetUser() -> GIDGoogleUser? {
        if let user = SocialManager.GoogleGetSharedInstance()?.currentUser {
            print("Social Manager - Google - User: Get Successfully")
            return user
        }
        print("Social Manager - Google - User: Not Found")
        return nil
    }
    
    static func GoogleLogout() -> Bool {
        GIDSignIn.sharedInstance().signOut()
        return true
    }
    
    //MARK: Facebook
    static func EnableFacebook(application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    static func FacebookLogin(from: UIViewController, permisions: [String], callback: ((LoginManagerLoginResult?, Error?) -> Void)?) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: permisions, from: from) { (loginResult, error) in
            if error != nil {
                print("Social Manager - Facebook - SignIn Error: \(error!.localizedDescription)")
                callback?(nil, error)
                return
            }
            if (loginResult?.isCancelled)! {
                print("Social Manager - Facebook - SignIn Error: Cancelled by user")
                let error = NSError.init(domain: "user", code: 1000, userInfo: [:])
                callback?(nil, error as Error)
                return
            }
            print("Social Manager - Facebook - SignIn User: Successfull")
            callback?(loginResult, nil)
        }
    }
    
    static func FacebookIsUserLogedIn() -> Bool {
        if AccessToken.current != nil {
            print("Social Manager - Facebook - User: Signed In")
            return true
        }
        else {
            print("Social Manager - Facebook - User: Not Signed In")
            return false
        }
    }
    
    static func FacebookGetUser(graphPath: String, parameters: [String: String], callback: ((GraphRequestConnection?, Any?, Error?) -> Void)?) {
        
        if SocialManager.FacebookIsUserLogedIn() {
            
            let graphRequest : GraphRequest = GraphRequest(graphPath: graphPath, parameters: parameters)
            
            graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                
                if ((error) != nil) {
                    print("Social Manager - Facebook - User: Error: \(error!.localizedDescription)")
                    callback?(nil, nil, error)
                }
                else {
                    print("Social Manager - Facebook - User: Get Successfully")
                    callback?(connection, result, nil)
                }
            })
            return
        }
        
        print("Social Manager - Facebook - SignIn User: Not Signed In")
        let error = NSError.init(domain: "user", code: 1000, userInfo: [:])
        callback?(nil, nil, error as Error)
        
    }
    
    static func FacebookLogout() -> Bool {
        if SocialManager.FacebookIsUserLogedIn() {
            LoginManager().logOut()
            return true
        }
        print("Social Manager - Facebook - User: Allready logout")
        return true
    }
}
