//
//  TeacherSocialColors.swift
//  Teachers Social
//
//  Created by Ratheesh Maithadam on 1/12/20.
//  Copyright © 2020 Abdul Muqeem. All rights reserved.
//



import Foundation
enum TeacherSocialColors: String {
    case tsColor1 = "#1de9b6"
    case tsColor2 = "#ff1744"
    case tsColor3 = "#00e676"
    case tsColor4 = "#ffc400"
    case tsColor5 = "#00e5ff"
    case tsColor6 = "#ff9100"
    case tsColor7 = "#3d5afe"
    case tsColor8 = "#d500f9"
    case tsColor9 = "#ff4081"
    case tsColor10 = "#2979ff"
    case tsColor11 = "#00b0ff"
    case tsColor12 = "#c6ff00"
    case tsColor13 = "#ffea00"
    case tsColor14 = "#ff3d00"
   
    static func colorRandom(randomValue:Int) -> TeacherSocialColors {
        switch randomValue {
        case 0:
            return .tsColor1
        case 1:
            return .tsColor2
        case 2:
            return .tsColor3
        case 3:
            return .tsColor4
        case 4:
            return .tsColor5
        case 5:
            return .tsColor6
        case 6:
            return .tsColor7
        case 7:
            return .tsColor8
        case 8:
            return .tsColor9
        case 9:
            return .tsColor10
        case 10:
            return .tsColor11
        case 11:
            return .tsColor12
        case 12:
            return .tsColor13
        case 13:
            return .tsColor14
        default:
            return .tsColor4
        }
    }
}
enum EventColors: String {
    
    case tsColor1 = "#0b9fd7"
    case tsColor2 = "#ff1744"
    case tsColor3 = "#d500f9"
    case tsColor4 = "#00e5ff"
    case tsColor5 = "#68d909"
    case tsColor6 = "#ffea00"
    case tsColor7 = "#ffc400"
    case tsColor8 = "#1de9b6"
    case tsColor9 = "#3d5afe"
    case tsColor10 = "#ff4081"
    case tsColor11 = "#2979ff"
    case tsColor12 = "#00b0ff"
    case tsColor13 = "#c6ff00"
    case tsColor14 = "#ffea04"
    
    static func colorRandom(randomValue:Int) -> EventColors {
        switch randomValue {
        case 0:
            return .tsColor1
        case 1:
            return .tsColor2
        case 2:
            return .tsColor3
        case 3:
            return .tsColor4
        case 4:
            return .tsColor5
        case 5:
            return .tsColor6
        case 6:
            return .tsColor7
        case 7:
            return .tsColor8
        case 8:
            return .tsColor9
        case 9:
            return .tsColor10
        case 10:
            return .tsColor11
        case 11:
            return .tsColor12
        case 12:
            return .tsColor13
        case 13:
            return .tsColor14
        default:
            return .tsColor4
        }
    }
}

enum WellbeingColors: String {
    case tsColor1 = "#00e676"
    case tsColor2 = "#ff1744"
    case tsColor3 = "#68d909"
    case tsColor4 = "#ffc400"
    case tsColor5 = "#00e5ff"
    case tsColor6 = "#ff9100"
    case tsColor7 = "#3d5afe"
    case tsColor8 = "#d500f9"
    case tsColor9 = "#ff4081"
    case tsColor10 = "#2979ff"
    case tsColor11 = "#00b0ff"
    case tsColor12 = "#c6ff00"
    case tsColor13 = "#ffea00"

    static func colorRandom(randomValue:Int) -> WellbeingColors {
        switch randomValue {
        case 0:
            return .tsColor1
        case 1:
            return .tsColor1
        case 2:
            return .tsColor2
        case 3:
            return .tsColor3
        case 4:
            return .tsColor5
        case 5:
            return .tsColor6
        case 6:
            return .tsColor7
        case 7:
            return .tsColor8
        case 8:
            return .tsColor9
        case 9:
            return .tsColor10
        case 10:
            return .tsColor11
        case 11:
            return .tsColor12
        case 12:
            return .tsColor13
        default:
            return .tsColor1
        }
    }
}
