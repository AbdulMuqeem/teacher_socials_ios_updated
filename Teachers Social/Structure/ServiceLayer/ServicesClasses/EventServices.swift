//
//  EventServices.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 26/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import Foundation
import SwiftyJSON

class EventServices {
    
    static func GetEventCategoryList(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.GetRequest(fromSavedUrl: ServiceApiEndPoints.eventCategoryList , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetEventList(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.eventList , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }

    static func GetSearchEvent(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.searchEvent , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func GetEventDetails(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.eventDetails , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func BookNowEvent(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.bookNow , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
    static func ConfirmationPin(param:[String:Any],completionHandler:@escaping (_ success:Bool,_ response:JSON?,_ error:Error?)->Void) {
        
        Request.PostRequest(fromSavedUrl: ServiceApiEndPoints.pin , parameters: param, callback: { (response, error) in
            
            if error != nil {
                print("Error: \(String(describing: (error?.localizedDescription)!))")
                completionHandler(false, nil, error)
                return
            }
            
            if response?["success"].boolValue != true {
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg!))")
                completionHandler(false, response!, nil)
                return
            }
            completionHandler(true, response!, nil)
        })
        
    }
    
}
