//
//  ServiceApiEndPoints.swift
//  TAAJ
//
//  Created by Abdul Muqeem on 22/03/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import Foundation

class ServiceApiEndPoints: NSObject  {
    
    //static let baseURL = "http://ec2-13-232-236-94.ap-south-1.compute.amazonaws.com:3000/teachers/"
    static let baseURL = "http://ec2-3-6-23-213.ap-south-1.compute.amazonaws.com:3000/teachers/"
    
    // User Module
    static let login = baseURL + "loginAndAuthenticate"
    static let register = baseURL + "register"
    static let activate = baseURL + "activate"
    static let resendToken = baseURL + "resendActivationToken"
    static let forgotPassword = baseURL + "forgotPassword"
    static let resetPassword = baseURL + "resetPassword"
    static let countryList = baseURL + "nationalities"
    
    //Social Login
    static let fblogin = baseURL + "fbSignUpAndLogin"
    static let googlelogin = baseURL + "googleSignUpAndLogin"
    static let fbRegister = baseURL + "fbSignUpMissingFields"
    static let googleRegister = baseURL + "googleSignUpMissingFields"
    
    //Profile
    static let getProfile = baseURL + "getProfileDetails"
    static let updateProfile = baseURL + "updateProfileDetails"
    static let updateProfileImage = baseURL + "updateProfilePicture"
    static let updateSmartPass = baseURL + "updateSmartPass"
    static let updatePassword = baseURL + "updatePassword"
    static let verification = baseURL + "verifyAccount"
    static let inviteFriend = baseURL + "inviteAFriend"
    static let relocation = baseURL + "submitRelocationForm"
    static let getAbout = baseURL + "aboutUs"
    static let getTerms = baseURL + "termsAndConditions"
    static let getSubscription = baseURL + "subscribe"
    static let getOfferMessage = baseURL + "usedOfferMessage"
    static let getSubscriptionMessage = baseURL + "getSubscriptionText"
    static let togglePushNotifications = baseURL + "togglePushNotifications"
    
    
    static let getActivities = baseURL + "activitiesHistory"
    static let feedback = baseURL + "submitFeedback"
    static let closeAccount = baseURL + "submitFeedbackAndCloseAccount"
    
    static let getMonth = baseURL + "getMonthYearList"
    static let getexpenseSheet = baseURL + "getExpenseSheet"
    static let addIncome = baseURL + "addIncome"
    static let addExpense = baseURL + "addExpense"
    static let editIncome = baseURL + "editIncome"
    static let deleteIncome = baseURL + "deleteIncome"
    static let editExpense = baseURL + "editExpense"
    static let deleteExpense = baseURL + "deleteExpense"
    
    //Offers
    static let offerCategoryList = baseURL + "getOfferCategories"
    static let searchOffer = baseURL + "searchOffers"
    static let offerList = baseURL + "viewOffers"
    static let offerDetails = baseURL + "viewOfferDetails"
    static let bookOffer = baseURL + "useOffer"
    
    //Event
    static let eventCategoryList = baseURL + "getEventsCategories"
    static let searchEvent = baseURL + "searchEvents"
    static let eventList = baseURL + "viewEvents"
    static let eventDetails = baseURL + "viewEventDetails"
    static let bookNow = baseURL + "buyEvent"
    static let pin = baseURL + "attendEvent"
    
    //Well Being
    static let wellBeing = baseURL + "viewTeacherBoardPostings"
    static let wellBeingDetails = baseURL + "viewTeacherBoardPostingDetail"
    
    
}
