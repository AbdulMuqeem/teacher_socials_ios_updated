//
//  ViewController.swift
//  QuizUp
//
//  Created by Admin on 27/06/2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit


extension TutorialViewController : ViewPagerDataSource , StartedDelegate {
    
    func startedAction() {
        let vc = LoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func numberOfItems(viewPager:ViewPager) -> Int {
        return 3
    }
    
    func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {

        let view = TutorialView.instanceFromNib() as? TutorialView
        
        if index == 0 {

            view?.lblTitle.text = "GET THE BEST \n OFFERS AROUND"
            view?.lblDescription.text = "Whether it's a discount at your favourite\nbar or resturant, an offer on your hotel\nstay or a deal on your gym membership\nTeacher Socials has you covered!"
            view?.imgBackground.image = UIImage(named: "tutorial_screen_bg1")
            view?.delegate = self
            
        } else if index == 1 {
        
            view?.lblTitle.text = "SURROUND YOURSELF \n WITH HAPPINESS"
            view?.lblDescription.text = "Teachers Socials membership is open to all\nindividuals working in the education\nsector and comes with a well-being\nsection which supports your financial,\nphysical and mental well-being!"
            view?.imgBackground.image = UIImage(named: "tutorial_screen_bg2")
            view?.delegate = self
       
        } else if index == 2 {
            
            view?.lblTitle.text = "ENTER THE LARGEST \n TEACHERS COMMUNITY \n IN THE REGION"
            view?.lblDescription.text = "Teacher Socials help teachers find a\nhealthy balance between work and life\noutside of the classroom, through our\ngroup activities and social events!"
            view?.imgBackground.image = UIImage(named: "tutorial_screen_bg3")
            view?.delegate = self

        }
        
        return view!
        
    }
}

func didSelectedItem(index: Int) {
    print("select index \(index)")
}

class TutorialViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> TutorialViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TutorialViewController
    }
    
    
    @IBOutlet weak var viewPager:ViewPager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set("true", forKey: "isFirstLaunched")

        self.viewPager.dataSource = self
        self.viewPager.pageControl.isUserInteractionEnabled = false

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.DarkStatusBar()
        self.navigationController?.isNavigationBarHidden = true
    }

}

