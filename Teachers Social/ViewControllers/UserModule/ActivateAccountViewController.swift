//
//  ActivateAccountViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 01/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ActivateAccountViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func doneAction() {
        self.termsView.isHidden = true
    }
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class ActivateAccountViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ActivateAccountViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ActivateAccountViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var termsView:TermsView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    
    @IBOutlet weak var txtOTP:UITextField!
    var email:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.termsView.delegate = self
        self.termsView.isHidden = true
        
        self.lblOne.text = "Activate Your\nAccount Today"
        self.lblTwo.text = "Please Enter the OTP sent on your\nemail to reset your password"
        
    }
    
    @IBAction func resendOTPAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["emailId":self.email]
        print(params)
        
        UserServices.ResendToken(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let msg = response!["data"]["successMessage"].string {
                self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: msg , id: 0)
                self.alertView.isHidden = false
            }
            
        })
        
    }
    
    @IBAction func activateAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let otp = self.txtOTP.text!
        
        if  otp.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter OTP" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["emailId":self.email , "activationToken":otp]
        print(params)
        
        UserServices.Activate(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let verify = response!["data"]["teacher"]["isVerified"].bool {
                if verify == false {
                    UserDefaults.standard.set("false", forKey: VERIFIED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: VERIFIED)
                }
            }
            
            if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                if submitted == false {
                    UserDefaults.standard.set("false", forKey: SUBMITTED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBMITTED)
                }
            }
            
            if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                if subscribe == false {
                    UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                }
            }
            
            let userResultObj = User(object:(response?["data"]["teacher"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            let vc = HomeOffersViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
        
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        self.getTermsContent()
    }
    
    func getTermsContent() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        ProfileServices.Terms(param: [:], completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let terms = response!["data"]["termsAndConditions"]["content"].string {
                if let privacy = response!["data"]["privacyPolicy"]["content"].string {
                    let text = terms + privacy
                    self.termsView.txtTerms.attributedText = text.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
                else {
                    self.termsView.txtTerms.attributedText = terms.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
            }
            
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.termsView.setView(view: self.termsView, hidden: true)
    }
    
}
