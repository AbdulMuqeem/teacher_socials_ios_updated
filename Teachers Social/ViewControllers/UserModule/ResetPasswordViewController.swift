//
//  ResetPasswordViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 01/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ResetPasswordViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func doneAction() {
        self.termsView.isHidden = true
    }
    
    func okAction() {
        
        if self.textAlert == "success" {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
        self.alertView.isHidden = true
    }
    
}

class ResetPasswordViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ResetPasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ResetPasswordViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var termsView:TermsView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    @IBOutlet weak var txtOTP:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.termsView.delegate = self
        self.termsView.isHidden = true
        
        self.lblOne.text = "Reset Your\nPassword"
        self.lblTwo.text = "No Problem.."
        self.lblThree.text = "Please Enter the OTP sent on your\nemail to reset your password"
        
        self.title = "Reset Password"
        self.navigationController?.isNavigationBarHidden = false
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        self.getTermsContent()
    }
    
    func getTermsContent() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        ProfileServices.Terms(param: [:], completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let terms = response!["data"]["termsAndConditions"]["content"].string {
                if let privacy = response!["data"]["privacyPolicy"]["content"].string {
                    let text = terms + privacy
                    self.termsView.txtTerms.attributedText = text.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
                else {
                    self.termsView.txtTerms.attributedText = terms.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
            }
            
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.termsView.setView(view: self.termsView, hidden: true)
    }
    
    @IBAction func resetAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let otp = self.txtOTP.text!
        let password = self.txtPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        
        if  otp.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter OTP" , style: .warning)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , style: .warning)
            return
        }
        
        if  confirmPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter confirm password" , style: .warning)
            return
        }
        
        if  password.count < 6 || confirmPassword.count < 6 {
            self.showBanner(title: "Alert", subTitle: "Please enter minimum 6 digit password" , style: .warning)
            return
            
        }
        
        if password != confirmPassword {
            self.showBanner(title: "Alert", subTitle: "Password & confirm password does not match" , style: .danger)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["password":password , "resetPasswordToken":otp]
        print(params)
        
        UserServices.ResetPassword(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let msg = response!["data"]["successMessage"].string {
                self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: msg , id: 0)
                self.alertView.isHidden = false
                self.textAlert = "success"
            }
            
        })
        
    }
    
}

