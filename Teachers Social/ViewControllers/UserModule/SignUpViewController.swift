//
//  SignUpViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 31/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

extension SignUpViewController: AlertViewDelegate , SuccessViewDelegate {
    
    func doneAction() {
        
        if self.textAlert == "fb" {
            
            self.isSocialLogin = true
            self.type = "fb"

            self.PasswordView.isHidden = true
            self.ConfirmPasswordView.isHidden = true
            
            if let name = self.Data["name"] as? String {
                self.txtName.text = name
                self.txtName.isUserInteractionEnabled = false
            }
            
            if let email = self.Data["email"] as? String {
                self.txtEmail.text = email
                self.txtEmail.isUserInteractionEnabled = false
            }
            
            if self.socialObj!.showEmail == true {
                self.txtEmail.text = ""
                self.txtEmail.isUserInteractionEnabled = true
            }

        }
        
        if self.textAlert == "google" {
            self.SetupSignupViewControllerForGoogleLogin()
        }
        
        self.termsView.isHidden = true
    }
    
    func okAction() {
        
        if self.textAlert == "register" {
            
            let vc = ActivateAccountViewController.instantiateFromStoryboard()
            vc.email = self.txtEmail.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if self.textAlert == "fbRegister" {
            
            let vc = HomeOffersViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        if self.textAlert == "google" {
            self.SetupSignupViewControllerForGoogleLogin()
        }
        
        self.alertView.isHidden = true
    }
    
}

class SignUpViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SignUpViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SignUpViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var termsView:TermsView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPin:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    @IBOutlet weak var PasswordView:UIView!
    @IBOutlet weak var ConfirmPasswordView:UIView!
    
    @IBOutlet weak var BoxViewHeight:BaseLayoutConstraint!
    @IBOutlet weak var MainViewHeight:BaseLayoutConstraint!
    
    var type:String = ""
    
    var isSocialLogin = false
    var socialObj:SocialLogin?
    var Data = [String: Any]()
    var userObj:User?
    
    var deviceId:String = ""
    var fbAccessToken:String = ""
    var googleAccessToken:String = ""
    var fbName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.termsView.delegate = self
        self.termsView.isHidden = true
        self.txtPin.delegate = self
        
        SocialManager.GoogleSetDelegate(delegate: self)
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        self.lblOne.text = "Welcome To\nTeacher Socials"
        self.lblTwo.text = "The Largest Teachers Community\nin the region"
        self.lblThree.text = "Hello Teacher\nLet's create your free account!"
        
        if self.isSocialLogin != false {
            
            self.PasswordView.isHidden = true
            self.ConfirmPasswordView.isHidden = true
            
            if let name = self.Data["name"] as? String {
                self.txtName.text = name
                self.txtName.isUserInteractionEnabled = false
            }
            
            if let email = self.Data["email"] as? String {
                self.txtEmail.text = email
                self.txtEmail.isUserInteractionEnabled = false
            }
            
            if self.socialObj!.showEmail == true {
                self.txtEmail.text = ""
                self.txtEmail.isUserInteractionEnabled = true
            }
            
        }
        
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        self.getTermsContent()
    }
    
    func getTermsContent() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        ProfileServices.Terms(param: [:], completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let terms = response!["data"]["termsAndConditions"]["content"].string {
                if let privacy = response!["data"]["privacyPolicy"]["content"].string {
                    let text = terms + privacy
                    self.termsView.txtTerms.attributedText = text.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
                else {
                    self.termsView.txtTerms.attributedText = terms.html2AttributedString
                    self.termsView.setView(view: self.termsView, hidden: false)
                }
            }
            
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.termsView.setView(view: self.termsView, hidden: true)
    }
        
    @IBAction func googleAction(_ sender : UIButton) {
        SocialManager.GoogleLogin()
    }
    
    @IBAction func signupAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if self.isSocialLogin == false {
            self.signup()
            return
        }
        else {
            if self.type == "fb" {
                self.signupWithFB()
            }
            else {
                self.signupWithGoogle()
            }
        }
    }
    
    func signupWithFB() {
        
        let name = self.txtName.text!
        let pin = self.txtPin.text!
        let accessToken = self.Data["token"]
//        let deviceID = self.Data["deviceId"]
        
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter name" , style: .warning)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        if  pin.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter pin" , style: .warning)
            return
        }
        
        if  pin.count != 4 {
            self.showBanner(title: "Alert", subTitle: "Please enter 4 digit pin" , style: .warning)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let fcmDeviceToken = UserDefaults.standard.object(forKey: NOTIFICATION_TOKEN_KEY) as? String
        
        let params:[String:Any] = ["access_token" :accessToken! , "name":name , "emailId" : email , "smartPass" : pin, "deviceToken":fcmDeviceToken!]
        print(params)
        
        
        UserServices.FBRegister(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.userObj = User(object:(response?["data"]["teacher"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: self.userObj!)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            if let verify = response!["data"]["teacher"]["isVerified"].bool {
                if verify == false {
                    UserDefaults.standard.set("false", forKey: VERIFIED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: VERIFIED)
                }
            }
            
            if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                if submitted == false {
                    UserDefaults.standard.set("false", forKey: SUBMITTED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBMITTED)
                }
            }
            
            if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                if subscribe == false {
                    UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                }
            }
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Congratulations!", msg: "Your Account has been created successfully" , id: 0)
            self.alertView.isHidden = false
            self.textAlert = "fbRegister"
            
        })
        
    }
    
    func signupWithGoogle() {
        
        let name = self.txtName.text!
        let pin = self.txtPin.text!
        let userId = self.Data["id"]
//        let deviceID = self.Data["deviceId"]
        
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter name" , style: .warning)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        if  pin.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter pin" , style: .warning)
            return
        }
        
        if  pin.count != 4 {
            self.showBanner(title: "Alert", subTitle: "Please enter 4 digit pin" , style: .warning)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let fcmDeviceToken = UserDefaults.standard.object(forKey: NOTIFICATION_TOKEN_KEY)
        
        let params:[String:Any] = ["userId" : userId! , "displayName":name , "emailId" : email , "smartPass" : pin, "deviceToken":fcmDeviceToken! ]
        print(params)
        
        
        UserServices.GoogleRegister(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.userObj = User(object:(response?["data"]["teacher"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: self.userObj!)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            if let verify = response!["data"]["teacher"]["isVerified"].bool {
                if verify == false {
                    UserDefaults.standard.set("false", forKey: VERIFIED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: VERIFIED)
                }
            }
            
            if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                if submitted == false {
                    UserDefaults.standard.set("false", forKey: SUBMITTED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBMITTED)
                }
            }
            
            if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                if subscribe == false {
                    UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                }
            }
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Congratulations!", msg: "Your Account has been created successfully" , id: 0)
            self.alertView.isHidden = false
            self.textAlert = "fbRegister"
            
        })
        
    }
    
    func signup() {
        
        let name = self.txtName.text!
        let pin = self.txtPin.text!
        let password = self.txtPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter name" , style: .warning)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        if  pin.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter pin" , style: .warning)
            return
        }
        
        if  pin.count != 4 {
            self.showBanner(title: "Alert", subTitle: "Please enter 4 digit pin" , style: .warning)
            return
            
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter password" , style: .warning)
            return
        }
        
        if  confirmPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter confirm password" , style: .warning)
            return
        }
        
        if  password.count < 6 || confirmPassword.count < 6 {
            self.showBanner(title: "Alert", subTitle: "Please enter minimum 6 digit password" , style: .warning)
            return
            
        }
        
        if password != confirmPassword {
            self.showBanner(title: "Alert", subTitle: "Password & confirm password does not match" , style: .danger)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let fcmDeviceToken = UserDefaults.standard.object(forKey: NOTIFICATION_TOKEN_KEY)
        
        let params:[String:Any] = ["name":name , "emailId" : email , "smartPass" : pin , "password":password, "deviceToken":fcmDeviceToken!]
        print(params)
        
        UserServices.Register(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
          
            var successMessage = "Your Account has been created successfully, Kindly check email & Activate your account"
            if let messageValue =   response!["data"]["successMessage"].string
            {
                successMessage = messageValue
            }
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Congratulations!", msg:successMessage  , id: 0)
            self.alertView.isHidden = false
            self.textAlert = "register"
            
        })
    }
}

extension SignUpViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 3 {
            
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 3
        }
        
        return true
        
    }
    
}

//MARK:- Facebook Login
extension SignUpViewController {
    
    @IBAction func facebookAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if let udid = UIDevice.current.identifierForVendor?.uuidString {
            self.deviceId = udid
            print("Device Id: \(deviceId)")
            
        }
        
        SocialManager.FacebookLogin(from: self, permisions: [ "public_profile", "email" ]) { (result, error) in
            
            if error != nil {
                let error = String(describing: (error as AnyObject).localizedDescription)
                print("Error: \(error)")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            let fcmDeviceToken = UserDefaults.standard.object(forKey: NOTIFICATION_TOKEN_KEY) as? String
            
            self.fbAccessToken = result!.token!.tokenString
            
            let params: [String: Any] = ["access_token": self.fbAccessToken , "deviceToken":fcmDeviceToken!]
            print(params)
            
            UserServices.FacebookLogin(param: params, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        let error = String(describing: (error as AnyObject).localizedDescription)
                        print("Error: \(error)")
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                        self.alertView.isHidden = false
                        self.stopLoading()
                        return
                    }
                    let msg = response?["errorMessage"].stringValue
                    print("Message: \(String(describing: msg))")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                print(response!)
                
                self.socialObj = SocialLogin(object:(response?["data"])!)
                
                if let redirect = self.socialObj!.redirectForm {
                    
                    if redirect == false {
                        
                        self.stopLoading()
                        
                        if let verify = response!["data"]["teacher"]["isVerified"].bool {
                            if verify == false {
                                UserDefaults.standard.set("false", forKey: VERIFIED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: VERIFIED)
                            }
                        }
                        
                        if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                            if submitted == false {
                                UserDefaults.standard.set("false", forKey: SUBMITTED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBMITTED)
                            }
                        }
                        
                        if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                            if subscribe == false {
                                UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                            }
                        }
                        
                        
                        let userResultObj = User(object:(response?["data"]["teacher"])!)
                        UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                        Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                        
                        let vc = HomeOffersViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        
                        self.getFBData()
                        
                    }
                }
            })
        }
    }
    
    func getFBData() {
        
        SocialManager.FacebookGetUser(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email "], callback: { (connection, result, error) in
            
            if error != nil {
                let error = String(describing: (error as AnyObject).localizedDescription)
                print("Error: \(error)")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            guard let userInfo = result as? [String: Any] else {
                return
            } //handle the error
            
            if let firstName = (userInfo["first_name"]) as? String {
                print(firstName)
                self.fbName = firstName
                self.Data["name"] = self.fbName
            }
            
            if let lastName = (userInfo["last_name"]) as? String {
                print(lastName)
                self.fbName = self.fbName + " " + lastName
                self.Data["name"] = self.fbName
            }
            
            if let id = (userInfo["id"]) as? String {
                print(id)
                self.Data["id"] = id
            }
            
            if let email = (userInfo["email"]) as? String {
                print(email)
                self.Data["email"] = email
            }
            
            self.Data["token"] = self.fbAccessToken
            self.Data["deviceId"] = self.deviceId
            
            self.stopLoading()
            
            if let msg = self.socialObj!.successMessage {
                self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Success!", msg: msg , id: 0)
                self.alertView.isHidden = false
                self.textAlert = "fb"
            }
            
        })
        
    }
    
}

//MARK:- Google Login
extension SignUpViewController :  GIDSignInDelegate {
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    // Present a view that prompts the user to sign in with Google
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if error != nil {
            let error = String(describing: (error as AnyObject).localizedDescription)
            print("Error: \(error)")
            self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
            self.alertView.isHidden = false
            self.stopLoading()
            return
        }
        else {
            // Perform any operations on signed in user here.
            let userId = user.userID
//            let idToken = user.authentication.idToken
//            let access_token = user.authentication.accessToken
            let fullName = user.profile.name
            let email = user.profile.email
                   
            
//            print("Access Token: \(access_token!)")
            print("User ID: \(userId!)")
//            print("ID Token: \(idToken!)")
            
            if let udid = UIDevice.current.identifierForVendor?.uuidString {
                self.deviceId = udid
                print("Device Id: \(deviceId)")
                
            }
            
//            self.googleAccessToken = access_token!
            
            let fcmDeviceToken = UserDefaults.standard.object(forKey: NOTIFICATION_TOKEN_KEY)
            
            let params: [String: Any] = ["userId": userId!, "emailId": email!, "deviceToken":fcmDeviceToken!]
            print(params)
            
            UserServices.GoogleLogin(param: params, completionHandler: {(status, response, error) in
                
                if !status {
                    if error != nil {
                        let error = String(describing: (error as AnyObject).localizedDescription)
                        print("Error: \(error)")
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                        self.alertView.isHidden = false
                        self.stopLoading()
                        return
                    }
                    let msg = response?["errorMessage"].stringValue
                    print("Message: \(String(describing: msg))")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                print(response!)
                self.socialObj = SocialLogin(object:(response?["data"])!)
                
                if let redirect = self.socialObj!.redirectForm {
                    
                    if redirect == false {
                        
                        self.stopLoading()
                        
                        if let verify = response!["data"]["teacher"]["isVerified"].bool {
                            if verify == false {
                                UserDefaults.standard.set("false", forKey: VERIFIED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: VERIFIED)
                            }
                        }
                        
                        if let submitted = response!["data"]["teacher"]["isSubmitted"].bool {
                            if submitted == false {
                                UserDefaults.standard.set("false", forKey: SUBMITTED)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBMITTED)
                            }
                        }
                        
                        if let subscribe = response!["data"]["teacher"]["isSubscribed"].bool {
                            if subscribe == false {
                                UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                            }
                            else {
                                UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                            }
                        }
                        
                        let userResultObj = User(object:(response?["data"]["teacher"])!)
                        UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                        Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                        
                        let vc = HomeOffersViewController.instantiateFromStoryboard()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        
                        self.Data["id"] = userId
                        self.Data["email"] = email
                        self.Data["token"] = self.googleAccessToken
                        self.Data["deviceId"] = self.deviceId
                        self.Data["name"] = fullName
                        
                        self.stopLoading()
                        
                        if let msg = self.socialObj!.successMessage {
                            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Success!", msg: msg , id: 0)
                            self.alertView.isHidden = false
                            self.textAlert = "google"
                        }
                    }
                }
                
                return
            })
            
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
        print("Error: \(error.localizedDescription)")
    }
    
}

//MARK:- Helper Functions
extension SignUpViewController {
    func SetupSignupViewControllerForGoogleLogin() {
        self.isSocialLogin = true
        self.type = "google"

        self.PasswordView.isHidden = true
        self.ConfirmPasswordView.isHidden = true
        
        if let name = self.Data["name"] as? String {
            self.txtName.text = name
            self.txtName.isUserInteractionEnabled = false
        }
        
        if let email = self.Data["email"] as? String {
            self.txtEmail.text = email
            self.txtEmail.isUserInteractionEnabled = false
        }
        
        if self.socialObj!.showEmail == true {
            self.txtEmail.text = ""
            self.txtEmail.isUserInteractionEnabled = true
        }
    }
}
