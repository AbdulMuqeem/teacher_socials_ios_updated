//
//  HomeViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 30/10/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension HomeOffersViewController : AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "offers" {
            self.txtSearch.text = ""
            self.GetOffersList(type: "")
        }
        
        self.alertView.isHidden = true
    }
    
}



class HomeOffersViewController : UIViewController,UITextFieldDelegate {
    
    class func instantiateFromStoryboard() -> HomeOffersViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeOffersViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var HeaderCollectionView:UICollectionView!
    @IBOutlet weak var OffersCollectionView:UICollectionView!
    @IBOutlet weak var tabBar:UITabBar!
    
    @IBOutlet weak var offerFlowLayout: UICollectionViewFlowLayout!
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    var offersCategoryListArray:[String] = [String]()
    var offersListArray:[OffersList] = [OffersList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        self.txtSearch.delegate = self
        self.TabBarInit()
        self.tabBar.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:OffersHeadersCell.self), bundle: nil)
        self.HeaderCollectionView.register(cell, forCellWithReuseIdentifier: "OffersHeadersCell")
        
        let Offer_cell = UINib(nibName:String(describing:HomeOffersCell.self), bundle: nil)
        self.OffersCollectionView.register(Offer_cell, forCellWithReuseIdentifier: "HomeOffersCell")
        
        self.OffersCollectionView.delegate = self
        self.OffersCollectionView.dataSource = self
        
        self.HeaderCollectionView.reloadData()
        self.OffersCollectionView.reloadData()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchOffers()
        return true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBar.selectedItem = self.tabBar.items![2] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        self.DarkStatusBar()
        self.OffersCategoryList()
        self.GetOffersList(type: "")
        
    }
    
    func OffersCategoryList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.offersCategoryListArray.removeAll()
        self.HeaderCollectionView.reloadData()
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        OfferServices.GetOfferCategoryList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let obj = OfferCategories(json: response!["data"])
            self.offersCategoryListArray = obj.offerCategories!
            // setting colors
            self.setOfferColors(categoryies: obj.offerCategories!)
            self.HeaderCollectionView.reloadData()
            
        })
        
    }
    
    //setting offer colors
    func setOfferColors(categoryies:[String])
    {
        var offerColors = [String : String]()
        var i = 0
        for item in categoryies
        {
            offerColors[item] = TeacherSocialColors.colorRandom(randomValue: i).rawValue
            i+=1
            if i==13
            {
                i=0
            }
        }
        offerColors["All"] = TeacherSocialColors.colorRandom(randomValue: i).rawValue
        
        ColorCodes.shared.offerColors = offerColors
        
    }
    
    func GetOffersList(type:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.offersListArray.removeAll()
        self.OffersCollectionView.reloadData()
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let fcmDeviceToken = UserDefaults.standard.object(forKey: NOTIFICATION_TOKEN_KEY)
        
        let id = Singleton.sharedInstance.CurrentUser!.id
        
        let param:[String:Any] = ["category":type , "teacherId":id!, "deviceToken":fcmDeviceToken!]
        print(param)
        
        OfferServices.GetOfferList(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?["data"]["offers"].arrayValue
            
            for resultObj in responseArray! {
                let obj =  OffersList(json: resultObj)
                self.offersListArray.append(obj)
            }
            
            if let verify = response!["data"]["isVerified"].bool {
                if verify == false {
                    UserDefaults.standard.set("false", forKey: VERIFIED)
                }
                else {
                    UserDefaults.standard.set("true", forKey: VERIFIED)
                }
            }
            
            if let subscribed = response!["data"]["isSubscribed"].bool {
                if subscribed == false {
                    UserDefaults.standard.set("false", forKey: SUBSCRIPTION)
                }
                else {
                    UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
                }
            }
            
            
            self.OffersCollectionView.reloadData()
            
        })
        
    }
    
    @IBAction func searchEventsAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        searchOffers()
    }
    
    func searchOffers()
    {
        
        let search = self.txtSearch.text!
        
        if search.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter keyword for search events" , style: .warning)
            return
        }
        
        self.offersListArray.removeAll()
        self.OffersCollectionView.reloadData()
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        
        let param:[String:Any] = ["queryString":search ]
        print(param)
        
        OfferServices.GetSearchOffer(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.textAlert = "offers"
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?["data"]["offers"].arrayValue
            
            for resultObj in responseArray! {
                let obj =  OffersList(json: resultObj)
                self.offersListArray.append(obj)
            }
            
            self.OffersCollectionView.reloadData()
            
        })
        
    }
}

extension HomeOffersViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.HeaderCollectionView {
            return self.offersCategoryListArray.count + 1
        }
        else {
            return self.offersListArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.HeaderCollectionView {
            
            var text = ""
            
            if indexPath.item == 0 {
                text = "All"
            }
            else {
                text = self.offersCategoryListArray[indexPath.row - 1]
            }
            
            let height = GISTUtility.convertToRatio(40)
            let font = UIFont(name: "HelveticaNeue", size: 14.0)!
            let width = self.estimatedFrame(text: text, font: font).width + 5
            
            return CGSize(width: width, height: height)
        }
        else {
            return CGSize(width: (self.OffersCollectionView.frame.size.width-17)/2, height: 270.0)
            
            //            let padding: CGFloat = 60.0
            //            let collectionViewSize = self.OffersCollectionView.frame.size.width - padding
            //            return CGSize(width: collectionViewSize/2, height: 270.0)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0 , left: 0 , bottom: 0 , right: 0)
        
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let height = GISTUtility.convertToRatio(40)
        let size = CGSize(width: 200, height: height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.HeaderCollectionView {
            
            var text = ""
            
            if indexPath.item == 0 {
                text = "All"
            }
            else {
                text = self.offersCategoryListArray[indexPath.row - 1]
            }
            
            let cell:OffersHeadersCell = collectionView.dequeueReusableCell(withReuseIdentifier:
                String(describing:OffersHeadersCell.self), for: indexPath) as! OffersHeadersCell
            
            
            
            cell.lblText.text = text
            
            // cell.bgView.backgroundColor = UIColor.random()
            //setting dynamic colors
            if ColorCodes.shared.offerColors.count > 0
            {
                if indexPath.item == 0
                {
                    cell.bgView.backgroundColor = UIColor.init(hex: ColorCodes.shared.offerColors["All"] ?? "All")
                }
                else
                {
                    cell.bgView.backgroundColor = UIColor.init(hex: ColorCodes.shared.offerColors[self.offersCategoryListArray[indexPath.row-1]] ?? "All")
                }
            }
            else
            {
                cell.bgView.backgroundColor = UIColor.random()
            }
            
            if indexPath.item == 0 {
                cell.lblText.text = "All"
                cell.bgView.backgroundColor = UIColor.init(hex: "#0b9fd7")
            }
            
            return cell
        }
        else {
            
            let obj = self.offersListArray[indexPath.row]
            
            let cell:HomeOffersCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeOffersCell.self), for: indexPath) as! HomeOffersCell
            
            if ColorCodes.shared.offerColors.count > 0
            {
                cell.bgView.backgroundColor = UIColor.init(hex: ColorCodes.shared.offerColors[obj.offerCategory!] ?? "All")
            }
            else
            {
                cell.bgView.backgroundColor = UIColor.random()
            }
            
            if let image = obj.offerPictureUrl {
                cell.imageView.setImageFromUrl(urlStr: image)
            }
            
            if let name = obj.offerName {
                cell.lblTitle.text = name
            }
            
            if let discount = obj.offerDiscount {
                cell.lblDiscount.text = discount
            }
            
            let subscription = UserDefaults.standard.object(forKey: SUBSCRIPTION) as? String
            
            if subscription == "false" {
                
                if obj.isFree == true {
                    cell.imgLock.isHidden = true
                }
                else {
                    cell.imgLock.isHidden = false
                }
                
            }
            else {
                cell.imgLock.isHidden = true
            }
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
        if collectionView == self.HeaderCollectionView {
            
            if indexPath.item == 0 {
                self.GetOffersList(type: "")
                return
            }
            
            let text = self.offersCategoryListArray[indexPath.row - 1]
            self.GetOffersList(type: text)
            return
        }
        else {
            
            let obj = self.offersListArray[indexPath.row]
            
            let subscription = UserDefaults.standard.object(forKey: SUBSCRIPTION) as? String
            
            if subscription == "false" {
                
                if obj.isFree == true {
                    let vc = OffersDetailViewController.instantiateFromStoryboard()
                    vc.modalPresentationStyle = .custom
                    vc.offersObj = obj
                    vc.offerID = obj.id!
                    self.present(vc, animated:true, completion: nil)
                }
                else {
                    /* self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "Kindly purchase subscription to access this offer", id: 0)
                     self.alertView.isHidden = false */
                    
                    let vc = MySubscriptionViewController.instantiateFromStoryboard()
                    //vc.delegate = self
                    vc.modalPresentationStyle = .custom
                    self.present(vc, animated:true, completion: nil)
                    
                }
            }
            else {
                let vc = OffersDetailViewController.instantiateFromStoryboard()
                vc.modalPresentationStyle = .custom
                vc.offersObj = obj
                vc.offerID = obj.id!
                self.present(vc, animated:true, completion: nil)
                
            }
        }
    }
}

extension HomeOffersViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 2
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            let vc = EventsViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            let vc = WellBeingViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            self.tabBar.selectedItem = self.tabBar.items![2] as UITabBarItem
            return
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            let vc = ConnectViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            let vc = ProfileViewController.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}
