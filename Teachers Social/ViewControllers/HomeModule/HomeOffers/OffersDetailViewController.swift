//
//  OffersDetailViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 26/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ImageSlideshow

extension OffersDetailViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class OffersDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> OffersDetailViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! OffersDetailViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var tabBar:UITabBar!
    @IBOutlet weak var scrollView:UIScrollView!
    
    @IBOutlet weak var OffersCollectionView:UICollectionView!
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDiscount:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblOfferType:UILabel!
    @IBOutlet weak var lblOfferCategory:UILabel!
    @IBOutlet weak var lblDates:UILabel!
    @IBOutlet weak var lblDetails:UILabel!
    @IBOutlet weak var lblLocation:UILabel!
    @IBOutlet weak var lblContact:UILabel!
    @IBOutlet weak var ShadowTextView:UIView!
    @IBOutlet weak var TextView:UIView!
    @IBOutlet weak var lblTextView:UITextView!
    
    @IBOutlet weak var exceptLabel: UILabel!
    @IBOutlet weak var imgSlider:ImageSlideshow!
    var alamofireSource:[KingfisherSource]? = [KingfisherSource]()
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" ,"connect_icon" , "profile_icon"]
    
    var offersObj: OffersList?
    var offerID:String = ""
    var offersListArray:[OtherOffers] = [OtherOffers]()
    var locationRedirect = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        // self.initData()
        self.OffersDetails(id: self.offerID)
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        let Offer_cell = UINib(nibName:String(describing:HomeOffersDetailCell.self), bundle: nil)
        self.OffersCollectionView.register(Offer_cell, forCellWithReuseIdentifier: "HomeOffersDetailCell")
        
    }
    
    func OffersDetails(id:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.offersListArray.removeAll()
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let param:[String:Any] = ["offerId":id]
        print(param)
        
        OfferServices.GetOfferDetails(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let obj = OfferDetails(json: response!["data"])
            self.initData(obj: obj)
            
        })
        
    }
    
    func initData(obj:OfferDetails) {
        
        self.view.endEditing(true)
        
        //        if let index = self.offersListArray.index(where: {$0.id == self.offersObj!.id}) {
        //            self.offersListArray.remove(at: index)
        //        }
        
        if let contactNumber = obj.contactNumber {
            self.lblContact.text = contactNumber
        }
        
        if let logo = obj.offer!.offerPictureUrl {
            self.imgLogo.setImageFromUrl(urlStr: logo)
        }
        
        if let images = obj.offer!.offerGallery {
            
            self.alamofireSource?.removeAll()
            
            for img in images {
                let almofireObj = KingfisherSource(urlString: img)
                self.alamofireSource?.append(almofireObj!)
            }
            
            self.imgSlider.setImageInputs(self.alamofireSource!)
            self.imgSlider.contentScaleMode = .scaleToFill
            
        }
        
        if let title = obj.offer!.offerName {
            self.lblTitle.text = title
        }
        
        if let discount = obj.offer!.offerDiscount {
            self.lblDiscount.text = discount
        }
        
        if let description = obj.offer!.offerDescription {
            self.lblDescription.text = description
        }
        
        if let offerType = obj.offer!.offerDiscount {
            self.lblOfferType.text = offerType
        }
        
        if let category = obj.offer!.offerCategory {
            self.lblOfferCategory.text = category
        }
        
        if let except = obj.offer!.except {
            self.lblDates.text =  except
            if except.isEmpty
            {
                self.exceptLabel.isHidden = true
            }
        }
        else
        {
            self.exceptLabel.isHidden = true
        }
        
        if let location = obj.offer!.locationText {
            self.lblDetails.text = location
        }
        
        if let redirect = obj.offer!.location {
            self.locationRedirect = redirect
        }
        
        if let count = obj.otherOffers {
            self.offersListArray = count
            self.lblLocation.text = "There are \(count.count) more offers"
        }
        
        self.scrollView.scrollToTop(animated: true)
        self.OffersCollectionView.scrollToTop(animated: true)
        self.OffersCollectionView.reloadData()
        
    }
    
    @IBAction func locationAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        guard let url = URL(string: self.locationRedirect) else {
            return
        }
        
        UIApplication.shared.open(url)
        
    }
    
    @IBAction func termsAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.ShadowTextView.isHidden = false
        self.TextView.setView(view: TextView, hidden: false)
        
        if let terms = self.offersObj!.offerTermsAndConditions {
            self.lblTextView.attributedText = terms.html2AttributedString
        }
        
    }
    
    @IBAction func closeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.ShadowTextView.isHidden = true
        self.TextView.setView(view: TextView, hidden: true)
    }
    
    @IBAction func redeemOffer(_ sender : UIButton) {
        self.view.endEditing(true)
        let vc = DiscountViewController.instantiateFromStoryboard()
        vc.isOffer = true
        vc.offerObj = self.offersObj
        let navVc = UINavigationController(rootViewController: vc)
        self.present(navVc, animated: true , completion: nil)
    }
    
    @IBAction func nextAction(_ sender : UIButton) {
        
        guard let indexPath = self.OffersCollectionView.indexPathsForVisibleItems.first.flatMap({
            IndexPath(item: $0.row + 1, section: $0.section)
        }), self.OffersCollectionView.cellForItem(at: indexPath) != nil else {
            return
        }
        
        self.OffersCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        
    }
    
    @IBAction func backAction(_ sender : UIButton) {
        
        guard let indexPath = self.OffersCollectionView.indexPathsForVisibleItems.first.flatMap({
            IndexPath(item: $0.row - 1 , section: $0.section)
        }), self.OffersCollectionView.cellForItem(at: indexPath) != nil else {
            return
        }
        
        self.OffersCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        
    }
    
}

extension OffersDetailViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.offersListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat = 50.0
        let collectionViewSize = self.OffersCollectionView.frame.size.width  - padding
        return CGSize(width: collectionViewSize/2, height: 200.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0 , left: 0 , bottom: 0 , right: 0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj = self.offersListArray[indexPath.row]
        
        let cell:HomeOffersDetailCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:HomeOffersDetailCell.self), for: indexPath) as! HomeOffersDetailCell
        
        cell.bgView.backgroundColor = UIColor.random()
        
        if let image = obj.offerPictureUrl {
            cell.imageView.setImageFromUrl(urlStr: image)
        }
        
        if let name = obj.offerName {
            cell.lblTitle.text = name
        }
        
        if let discount = obj.offerDiscount {
            cell.lblDiscount.text = discount
        }
        
        let subscription = UserDefaults.standard.object(forKey: SUBSCRIPTION) as? String
        
        if subscription == "false" {
            
            if obj.isFree == true {
                cell.imgLock.isHidden = true
            }
            else {
                cell.imgLock.isHidden = false
            }
            
        }
        else {
            cell.imgLock.isHidden = true
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.offersListArray[indexPath.row]
        
        let subscription = UserDefaults.standard.object(forKey: SUBSCRIPTION) as? String
        
        if subscription == "false" {
            
            if obj.isFree == true {
                
                self.startLoading(message: "")
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2 ) {
                    self.OffersDetails(id: obj.id!)
                    self.setEditing(true, animated: true)
                    self.stopLoading()
                }
                
            }
            else {
                self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "Kindly purchase subscription to access this offer", id: 0)
                self.alertView.isHidden = false
            }
        }
        else {
            
            self.startLoading(message: "")
            
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2 ) {
                self.OffersDetails(id: obj.id!)
                self.setEditing(true, animated: true)
                self.stopLoading()
            }
        }
    }
}


extension OffersDetailViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.selectedItem = self.tabBar.items![2] as UITabBarItem
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 2
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            self.dismiss(animated: true, completion: nil)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            self.dismiss(animated: true, completion: nil)
            
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            self.dismiss(animated: true, completion: nil)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            self.dismiss(animated: true, completion: nil)
            
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
