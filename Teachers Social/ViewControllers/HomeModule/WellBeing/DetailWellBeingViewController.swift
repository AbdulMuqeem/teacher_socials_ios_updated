//
//  DetailWellBeingViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 26/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import WebKit
extension DetailWellBeingViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class DetailWellBeingViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> DetailWellBeingViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailWellBeingViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var txtView:UITextView!
    @IBOutlet weak var tabBar:UITabBar!
    @IBOutlet weak var webView: WKWebView!
    var WellBeingObj:WellBeingList?
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.LightStatusBar()
        self.wellBiengDetails()
        
        if let title = self.WellBeingObj!.postTitle {
            self.lblTitle.text = title
        }
        
        if let image = self.WellBeingObj!.postPictureUrl {
            self.imgBanner.setImageFromUrl(urlStr: image)
        }
        
        if let text = self.WellBeingObj!.postText {
            //self.txtView.attributedText = text.html2AttributedString
    
           //  let headerString = "<header><meta name='viewport'initial-scale=1.0,user-scalable=no'></header>"
            //self.webView.loadHTMLString(headerString+text , baseURL: nil)
            
            let html = """
            <html>
            <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <style> body { font-size: 100%; } </style>
            </head>
            <body>
            \(text)
            </body>
            </html>
            """
            self.webView.loadHTMLString(html, baseURL: nil)
           
        }
        
    }
    
    func wellBiengDetails() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = self.WellBeingObj!.id
        
        let param:[String:Any] = ["postingId":id!]
        print(param)
        
        WellBeingServices.GetWellBiengDetails(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
        })
        
    }
    
}

extension DetailWellBeingViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.selectedItem = self.tabBar.items![1] as UITabBarItem
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 1
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}

