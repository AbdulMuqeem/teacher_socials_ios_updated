//
//  WellBeingViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 04/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension WellBeingViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class WellBeingViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> WellBeingViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WellBeingViewController
    }
    
    @IBOutlet weak var HeaderCollectionView:UICollectionView!
    @IBOutlet weak var tblView:UITableView!
    
    @IBOutlet weak var tabBar:UITabBar!
    var WellBeingListArray:[WellBeingList]? = [WellBeingList]()
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    // let titlesArray = ["All Wellbeing" , "Physical" , "Mental" , "Financial"]
    let titlesArray = ["All Wellbeing" , "Mental" , "Physical" , "Financial"]
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:OffersHeadersCell.self), bundle: nil)
        self.HeaderCollectionView.register(cell, forCellWithReuseIdentifier: "OffersHeadersCell")
        
        let wellbeing_cell = UINib(nibName:String(describing:WellBeingCell.self), bundle: nil)
        self.tblView.register(wellbeing_cell, forCellReuseIdentifier: String(describing: WellBeingCell.self))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBar.selectedItem = self.tabBar.items![1] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        self.DarkStatusBar()
        self.getWellBeing(type: "")
    }
    
    func getWellBeing(type:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.WellBeingListArray?.removeAll()
        self.tblView.reloadData()
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let param:[String:Any] = ["category":type]
        print(param)
        
        WellBeingServices.GetWellBiengList(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?["data"]["teachersboards"].arrayValue
            
            for resultObj in responseArray! {
                let obj =  WellBeingList(json: resultObj)
                self.WellBeingListArray?.append(obj)
            }
            
            self.tblView.reloadData()
        })
    }
}

extension WellBeingViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let text = self.titlesArray[indexPath.row]
        
        let height = GISTUtility.convertToRatio(40)
        let font = UIFont(name: "HelveticaNeue", size: 14.0)!
        let width = self.estimatedFrame(text: text, font: font).width + 5
        
        return CGSize(width: width, height: height)
        
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let height = GISTUtility.convertToRatio(40)
        let size = CGSize(width: 200, height: height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let text = self.titlesArray[indexPath.row]
        
        let cell:OffersHeadersCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:OffersHeadersCell.self), for: indexPath) as! OffersHeadersCell
        
        cell.lblText.text = text
        
        
        cell.bgView.backgroundColor = UIColor.init(hex: WellbeingColors.colorRandom(randomValue: indexPath.row).rawValue)
        
        if indexPath.row == 0
        {
            cell.bgView.backgroundColor = UIColor.init(hex: "0b9fd7")
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
        if indexPath.row == 0 {
            self.getWellBeing(type: "")
            return
        }
        
        if indexPath.row == 1 {
            self.getWellBeing(type: "Physical")
            return
        }
        
        if indexPath.row == 2 {
            self.getWellBeing(type: "Mental")
            return
        }
        
        if indexPath.row == 3 {
            self.getWellBeing(type: "Financial")
            return
        }
        
    }
}

extension WellBeingViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.WellBeingListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(220)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = self.WellBeingListArray![indexPath.row]
        
        let cell:WellBeingCell = tableView.dequeueReusableCell(withIdentifier:String(describing:WellBeingCell.self)) as!  WellBeingCell
        
        if let image = obj.postPictureUrl {
            cell.imgBanner.setImageFromUrl(urlStr: image)
        }
        
        if let name = obj.postTitle {
            cell.lblTitle.text = name
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        let obj = self.WellBeingListArray![indexPath.row]
        
        let vc = DetailWellBeingViewController.instantiateFromStoryboard()
        vc.WellBeingObj = obj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension WellBeingViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 1
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            self.tabBar.selectedItem = self.tabBar.items![1] as UITabBarItem
            return
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
