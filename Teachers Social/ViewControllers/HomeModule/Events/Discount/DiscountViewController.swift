//
//  DiscountViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 28/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import PinCodeTextField

extension DiscountViewController : AlertViewDelegate , SuccessViewDelegate  {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        if self.textAlert == "offer" {
            let vc = DiscountSuccessfullViewController.instantiateFromStoryboard()
            vc.isOfferScreen = true
            vc.offerObj = self.offerObj
            vc.activitiesObj = self.activityObj
            vc.offerSuccessMessage = successMessage
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        else
        {
            
            let vc = DiscountSuccessfullViewController.instantiateFromStoryboard()
            vc.profileObj = self.profileObj
            vc.activitiesObj = self.activityObj
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

class DiscountViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> DiscountViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DiscountViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var txtPinCode:PinCodeTextField!
    
    @IBOutlet weak var tabBar:UITabBar!
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    var profileObj:ProfileDetails?
    var activityObj:ActivitiesHistory?
    
    var isOffer:Bool = false
    var offerObj:OffersList?
    
    var pinValue:String?
    var successMessage:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.txtPinCode.delegate = self
        self.txtPinCode.keyboardType = .numberPad
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        self.LightStatusBar()
        
        if self.isOffer == true {
            self.getOfferDetails()
        }
        self.initData()
        
        self.lblOne.text = "Please present this to unlock your\noffer!"
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    func initData() {
        
        if self.isOffer == true {
            self.lblName.text = Singleton.sharedInstance.CurrentUser!.name
            self.imgUser.setImageFromUrl(urlStr: Singleton.sharedInstance.CurrentUser!.profilePicture!)
            return
        }
        
        if self.profileObj == nil {
            return
        }
        
        if let image = self.profileObj!.profilePicture {
            self.imgUser.setImageFromUrl(urlStr: image)
        }
        
        if let name = self.profileObj!.name {
            self.lblName.text = name
        }
        
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -100 // Move view 100 points upward
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    func bookNowAction(pin:String) {
        
        self.view.endEditing(true)
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Whoops!", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let eventId = self.activityObj!.buyEventId
        
        let param:[String:Any] = ["buyEventId": eventId! , "confirmationPin":pin]
        print(param)
        
        EventServices.ConfirmationPin(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Whoops!", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    func OfferBookAction(pin:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Whoops!", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id
        let vendorId = self.offerObj!.vendorId
        let offerId = self.offerObj!.id
        
        let param:[String:Any] = ["vendorId": vendorId! , "approvalCode":pin , "offerId":offerId! , "teacherId":id!]
        print(param)
        
        OfferServices.BookOffers(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Whoops!", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
                self.textAlert = "offer"
                self.successMessage = message
            }
            
        })
        
    }
    
    func getOfferDetails() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Whoops!", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = self.offerObj!.id!
        
        let param:[String:Any] = ["offerId":id]
        print(param)
        
        ProfileServices.GetOfferMessage(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Whoops!", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let message = response?["data"]["offerMessage"]["offerDiscount"].stringValue
            self.lblTwo.text = message
            
        })
        
    }
    
    @IBAction func doneAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if self.pinValue?.count != 4 {
            self.showBanner(title: "Alert", subTitle: "kindly enter 4 digit pin" , style: .danger)
            return
        }
        
        if self.isOffer == false {
            self.bookNowAction(pin: self.pinValue!)
        }
        else {
            self.OfferBookAction(pin: self.pinValue!)
        }
    }
}

extension DiscountViewController: PinCodeTextFieldDelegate {
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
    }
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        
        self.pinValue = textField.text!
        print(self.pinValue!)
        
        return true
        
    }
}

extension DiscountViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
