//
//  DiscountSuccessfullViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 28/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension DiscountSuccessfullViewController : AlertViewDelegate  {
    
    func okAction() {
        self.alertView.isHidden = true
    }
}

class DiscountSuccessfullViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> DiscountSuccessfullViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DiscountSuccessfullViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var lblDiscount:UILabel!
    @IBOutlet weak var imgLogo:UIImageView!
    
    var profileObj:ProfileDetails?
    var activitiesObj:ActivitiesHistory?
    var offerObj:OffersList?
    var isOffer:Bool! = false
    var isOfferScreen:Bool! = false
    var offerSuccessMessage: String?
    
    @IBOutlet weak var tabBar:UITabBar!
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.initData()
        
    }
    
    func initData() {
        
        /*if self.isOfferScreen == true {
         if self.offerObj != nil {
         if let id = self.offerObj!.id {
         self.getOfferDetails(id: Int(id)!)
         return
         }
         }
         }*/
        
        if self.isOfferScreen == true {
            if self.offerObj != nil {
                self.lblDiscount.text = offerSuccessMessage
                self.lblName.text = "Congratulations\n\(Singleton.sharedInstance.CurrentUser!.name!)"
                if self.isOfferScreen == true {
                    self.imgLogo.setImageFromUrl(urlStr: self.offerObj!.offerPictureUrl!)
                }
                return
            }
            
        }
        
        
        if self.isOffer == true {
            
            if self.activitiesObj != nil {
                if let id = self.activitiesObj!.usedOfferId {
                    self.getOfferDetails(id: id)
                    return
                }
            }
        }
        
        if self.profileObj == nil {
            return
        }
        
        if let image = self.activitiesObj!.eventPictureUrl {
            self.imgLogo.setImageFromUrl(urlStr: image)
        }
        
        if let name = self.profileObj!.name {
            self.lblName.text = "Congratulations \(name)!"
        }
        
        if self.activitiesObj!.isAttended == true {
            
            self.lblMessage.text = "You have attended this event"
            self.lblDiscount.text = "we hope you had a great time"
            
        }
        else {
            
            self.lblMessage.text = "Welcome to the event!"
            self.lblDiscount.text = "Have a great time!"
            
        }
        
    }
    
    func getOfferDetails(id:Int) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let param:[String:Any] = ["usedOfferId":id]
        print(param)
        
        ProfileServices.GetOfferMessage(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let message = response?["data"]["offerMessage"]["offerDiscount"].stringValue
            //self.lblMessage.text = "you used this offer to get"
            self.lblMessage.text = " "
            self.lblDiscount.text = message
            
            self.lblName.text = "Congratulation\n\(Singleton.sharedInstance.CurrentUser!.name!)"
            if self.isOfferScreen == true {
                self.imgLogo.setImageFromUrl(urlStr: self.offerObj!.offerPictureUrl!)
            }
            else {
                self.imgLogo.setImageFromUrl(urlStr: self.activitiesObj!.offerPictureUrl!)
            }
        })
    }
}


extension DiscountSuccessfullViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
