//
//  EventsViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 04/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension EventsViewController : AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "events" {
            self.txtSearch.text = ""
            self.GetEventList(type: "")
        }
        
        self.alertView.isHidden = true
        
    }
}

class EventsViewController: UIViewController,UITextFieldDelegate {
    
    class func instantiateFromStoryboard() -> EventsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EventsViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var HeaderCollectionView:UICollectionView!
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var tabBar:UITabBar!
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    var categoryListArray:[String] = [String]()
    var eventsListArray:[EventsList] = [EventsList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        self.txtSearch.delegate = self
        self.TabBarInit()
        self.tabBar.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:OffersHeadersCell.self), bundle: nil)
        self.HeaderCollectionView.register(cell, forCellWithReuseIdentifier: "OffersHeadersCell")
        
        let event_cell = UINib(nibName:String(describing:EventsCell.self), bundle: nil)
        self.tblView.register(event_cell, forCellReuseIdentifier: String(describing: EventsCell.self))
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchEvents()
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBar.selectedItem = self.tabBar.items![0] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        self.DarkStatusBar()
        self.CategoryList()
        self.GetEventList(type: "")
        
    }
    
    func CategoryList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.categoryListArray.removeAll()
        self.HeaderCollectionView.reloadData()
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        EventServices.GetEventCategoryList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let obj = EventCategories(json: response!["data"])
            self.categoryListArray = obj.eventCategories!
            self.HeaderCollectionView.reloadData()
            
        })
        
    }
    
    func GetEventList(type:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.eventsListArray.removeAll()
        self.tblView.reloadData()
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let param:[String:Any] = ["category":type]
        print(param)
        
        EventServices.GetEventList(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?["data"]["events"].arrayValue
            
            for resultObj in responseArray! {
                let obj =  EventsList(json: resultObj)
                self.eventsListArray.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
    @IBAction func searchEventsAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        searchEvents()
    }
    
    func searchEvents()
    {
        let search = self.txtSearch.text!
        
        if search.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter keyword for search events" , style: .warning)
            return
        }
        
        self.eventsListArray.removeAll()
        self.tblView.reloadData()
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let param:[String:Any] = ["queryString":search]
        print(param)
        
        EventServices.GetSearchEvent(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.textAlert = "events"
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let responseArray = response?["data"]["events"].arrayValue
            
            for resultObj in responseArray! {
                let obj =  EventsList(json: resultObj)
                self.eventsListArray.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
}

extension EventsViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryListArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var text = ""
        
        if indexPath.item == 0 {
            text = "All"
        }
        else {
            text = self.categoryListArray[indexPath.row - 1]
        }
        
        let height = GISTUtility.convertToRatio(40)
        let font = UIFont(name: "HelveticaNeue", size: 14.0)!
        let width = self.estimatedFrame(text: text, font: font).width + 5
        
        return CGSize(width: width, height: height)
        
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let height = GISTUtility.convertToRatio(40)
        let size = CGSize(width: 200, height: height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var text = ""
        
        if indexPath.item == 0 {
            text = "All"
        }
        else {
            text = self.categoryListArray[indexPath.row - 1]
        }
        
        let cell:OffersHeadersCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:OffersHeadersCell.self), for: indexPath) as! OffersHeadersCell
        
        
        if indexPath.item == 0 {
            cell.lblText.text = "All"
            cell.bgView.backgroundColor = UIColor.random()
        }
        
        cell.lblText.text = text
        
        if indexPath.row < 13
        {
            cell.bgView.backgroundColor = UIColor.init(hex: EventColors.colorRandom(randomValue: indexPath.row).rawValue)
        }
        else
        {
            cell.bgView.backgroundColor = UIColor.random()
        }
        if indexPath.row == 0
        {
            cell.bgView.backgroundColor = UIColor.init(hex: "0b9fd7")
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
        if indexPath.item == 0 {
            self.GetEventList(type: "")
            return
        }
        
        let text = self.categoryListArray[indexPath.row - 1]
        self.GetEventList(type: text)
        
    }
    
}

extension EventsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.eventsListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(220)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = self.eventsListArray[indexPath.row]
        
        let cell:EventsCell = tableView.dequeueReusableCell(withIdentifier:String(describing:EventsCell.self)) as!  EventsCell
        
        if let image = obj.eventPictureUrl {
            cell.imgBanner.setImageFromUrl(urlStr: image)
        }
        
        if let name = obj.title {
            cell.lblTitle.text = name
        }
        
        if let category = obj.eventCategory {
            cell.lblCategory.text = category
        }
        
        if let price = obj.price {
            cell.lblPrice.text = "\(price) AED per person"
        }
        
        if let date = obj.eventTimestamp {
            cell.lblDate.text = date.GetTime(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", toFormat: "dd-MM-yyyy")
        }
        
        cell.imgLock.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        let obj = self.eventsListArray[indexPath.row]
        
        let vc = EventsDetailViewController.instantiateFromStoryboard()
        vc.eventObj = obj
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension EventsViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 0
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            self.tabBar.selectedItem = self.tabBar.items![0] as UITabBarItem
            return
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ProfileViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
    }
}
