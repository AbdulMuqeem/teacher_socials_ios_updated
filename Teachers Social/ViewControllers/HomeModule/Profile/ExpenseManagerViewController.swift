//
//  ExpenseManagerViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 04/12/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown

extension ExpenseManagerViewController : AlertViewDelegate  , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.successView.isHidden = true
        self.GetExpenseList()
    }
    
}

class ExpenseManagerViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ExpenseManagerViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ExpenseManagerViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblText:UILabel!
    @IBOutlet weak var lblBalance:UILabel!
    @IBOutlet weak var lblTotalIncome:UILabel!
    @IBOutlet weak var lblTotalExpense:UILabel!
    
    @IBOutlet weak var MonthView:UIView!
    @IBOutlet weak var lblMonth:UILabel!
    
    @IBOutlet weak var incomeTableView:UITableView!
    @IBOutlet weak var expenseTableView:UITableView!
    
    @IBOutlet weak var shadowView:UIView!
    @IBOutlet weak var incomeView:UIView!
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var txtTitle:UITextField!
    @IBOutlet weak var txtAmount:UITextField!
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var tabBar:UITabBar!    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    var MonthDropDown = DropDown()
    var MonthNameList = [String]()
    var incomeListArray:[Income] = [Income]()
    var expenseListArray:[Expenses] = [Expenses]()
    var type:String = ""
    var typeId:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTotalIncome.text = "Income:  AED"
        self.lblTotalExpense.text = "Expense:  AED"
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.lblText.text = "Track all your expenses and\nsave more!"
        
        self.getMonthDetails()
        
        // Month DropDown
        self.MonthDropDown.anchorView = self.MonthView // UIView or UIBarButtonItem
        self.MonthDropDown.bottomOffset = CGPoint(x: 0 , y: (self.MonthDropDown.anchorView?.plainView.bounds.height)!)
        self.MonthDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.lblMonth.text = item
            self.GetExpenseList()
            
        }
        
        //Register Cell
        let cell = UINib(nibName:String(describing:ExpenseCell.self), bundle: nil)
        self.incomeTableView.register(cell, forCellReuseIdentifier: String(describing: ExpenseCell.self))
        
        let cell1 = UINib(nibName:String(describing:ExpenseCell.self), bundle: nil)
        self.expenseTableView.register(cell1, forCellReuseIdentifier: String(describing: ExpenseCell.self))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.DarkStatusBar()
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    

    @IBAction func onClickDeleteAction(_ sender: Any) {
        
        self.view.endEditing(true)
      if self.type == "Edit Income" {
         
            self.deleteIncome()
        }
        else if self.type == "Edit Expense" {
            self.deleteExpense()
        }
        
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -100 // Move view 100 points upward
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0 // Move view to original position
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        self.shadowView.isHidden = true
        self.incomeView.setView(view: incomeView, hidden: true)
        
    }
    
    @IBAction func monthAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.MonthDropDown.show()
        
    }
    
    func getMonthDetails() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id]
        print(param)
        
        ProfileServices.GetMonthYear(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let obj =  ExpenseMonthYear(json: response!["data"])
            self.MonthNameList = obj.monthYearList!
            self.MonthDropDown.dataSource = self.MonthNameList
            
            self.lblMonth.text = self.MonthNameList.last
            self.GetExpenseList()
            
        })
        
    }
    
    func GetExpenseList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        self.incomeListArray.removeAll()
        self.expenseListArray.removeAll()
        
        self.incomeTableView.reloadData()
        self.expenseTableView.reloadData()
        
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id , "monthYear" : self.lblMonth.text!]
        print(param)
        
        ProfileServices.GetExpenseSheet(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let response = ExpenseManager(object:(response?["data"])!)
            
            if let balance = response.outstandingBalance {
                self.lblBalance.text = "\(balance.decimal) AED"
            }
            else {
                self.lblBalance.text = " AED"
            }
            
            if let income = response.totalIncome {
                self.lblTotalIncome.text = "Income: \(income.decimal) AED"
            }
            else {
                self.lblTotalIncome.text = "Income:  AED"
            }
            
            if let expense = response.totalExpense {
                self.lblTotalExpense.text = "Expense: \(expense.decimal) AED"
            }
            else {
                self.lblTotalExpense.text = "Expense:  AED"
            }
            
            if let incomeArray = response.income {
                self.incomeListArray = incomeArray
            }
            
            if let expenseArray = response.expenses {
                self.expenseListArray = expenseArray
            }
            
            self.incomeTableView.reloadData()
            self.expenseTableView.reloadData()
            
        })
        
    }
    
    @IBAction func addIncomeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.type = "Add Income"
        self.lblTitle.text = "Add Income"
        self.txtTitle.text = ""
        self.txtAmount.text = ""
        self.shadowView.isHidden = false
        self.deleteBtn.isHidden = true
        self.incomeView.setView(view: incomeView, hidden: false)
    }
    
    @IBAction func addExpenseAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.type = "Add Expense"
        self.lblTitle.text = "Add Expense"
        self.txtTitle.text = ""
        self.txtAmount.text = ""
        self.shadowView.isHidden = false
        self.deleteBtn.isHidden = true
        self.incomeView.setView(view: incomeView, hidden: false)
    }
    
    @IBAction func submitAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        if self.type == "Add Income" {
            self.addIncome()
        }
        else if self.type == "Add Expense" {
            self.addExpense()
        }
        else if self.type == "Edit Income" {
         
            self.editIncome()
        }
        else if self.type == "Edit Expense" {
            self.editExpense()
        }
        
    }
    
    func addIncome() {
        
        self.view.endEditing(true)
        
        let title = self.txtTitle.text!
        let amount = self.txtAmount.text!
        let month = self.lblMonth.text!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        if  title.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter title" , style: .warning)
            return
        }
        
        if  amount.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter amount" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        
        let param:[String:Any] = ["teacherId":id , "monthYear":month , "incomeName":title , "incomeAmount":amount]
        print(param)
        
        ProfileServices.AddIncome(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.shadowView.isHidden = true
            self.incomeView.setView(view: self.incomeView, hidden: true)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    func addExpense() {
        
        self.view.endEditing(true)
        
        let title = self.txtTitle.text!
        let amount = self.txtAmount.text!
        let month = self.lblMonth.text!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        if  title.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter title" , style: .warning)
            return
        }
        
        if  amount.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter amount" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        
        let param:[String:Any] = ["teacherId":id , "monthYear":month , "expenseName":title , "expenseAmount":amount]
        print(param)
        
        ProfileServices.AddExpense(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.shadowView.isHidden = true
            self.incomeView.setView(view: self.incomeView, hidden: true)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    func editIncome() {
        self.view.endEditing(true)
        
        let title = self.txtTitle.text!
        let amount = self.txtAmount.text!
        let month = self.lblMonth.text!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        if  title.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter title" , style: .warning)
            return
        }
        
        if  amount.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter amount" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        
        let param:[String:Any] = ["teacherId":id , "monthYear":month , "incomeName":title , "incomeAmount":amount , "entryId":self.typeId]
        print(param)
        
        ProfileServices.EditIncome(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.shadowView.isHidden = true
            self.incomeView.setView(view: self.incomeView, hidden: true)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    func editExpense() {
        self.view.endEditing(true)
        
        let title = self.txtTitle.text!
        let amount = self.txtAmount.text!
        let month = self.lblMonth.text!
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        if  title.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter title" , style: .warning)
            return
        }
        
        if  amount.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter amount" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        
        let param:[String:Any] = ["teacherId":id , "monthYear":month , "expenseName":title , "expenseAmount":amount , "entryId":self.typeId]
        print(param)
        
        ProfileServices.EditExpense(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.shadowView.isHidden = true
            self.incomeView.setView(view: self.incomeView, hidden: true)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
    }
    
    
    
    func deleteIncome() {
          self.view.endEditing(true)
          let month = self.lblMonth.text!
          let id = Singleton.sharedInstance.CurrentUser!.id!
          
          self.startLoading(message: "")
          
          if !AppHelper.isConnectedToInternet() {
              self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
              self.stopLoading()
              return
          }
          
          
          let param:[String:Any] = ["teacherId":id , "monthYear":month ,  "entryId":self.typeId]
          print(param)
          
          ProfileServices.deleteIncome(param: param , completionHandler: {(status, response, error) in
              
              if !status {
                  if error != nil {
                      let error = String(describing: (error as AnyObject).localizedDescription)
                      print("Error: \(error)")
                      self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                      self.alertView.isHidden = false
                      self.stopLoading()
                      return
                  }
                  let msg = response?["errorMessage"].stringValue
                  print("Message: \(String(describing: msg))")
                  self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                  self.alertView.isHidden = false
                  self.stopLoading()
                  return
              }
              
              self.stopLoading()
              print(response!)
              
              self.shadowView.isHidden = true
              self.incomeView.setView(view: self.incomeView, hidden: true)
              
              if let message = response!["data"]["successMessage"].string {
                  self.successView.alertShow(title: "Great Job!", msg: message)
                  self.successView.isHidden = false
              }
              
          })
          
      }
    
    
    func deleteExpense() {
           self.view.endEditing(true)
           let month = self.lblMonth.text!
           let id = Singleton.sharedInstance.CurrentUser!.id!
           self.startLoading(message: "")
           
           if !AppHelper.isConnectedToInternet() {
               self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
               self.stopLoading()
               return
           }
           
           
           let param:[String:Any] = ["teacherId":id , "monthYear":month , "entryId":self.typeId]
           print(param)
           
           ProfileServices.deleteExpense(param: param , completionHandler: {(status, response, error) in
               
               if !status {
                   if error != nil {
                       let error = String(describing: (error as AnyObject).localizedDescription)
                       print("Error: \(error)")
                       self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                       self.alertView.isHidden = false
                       self.stopLoading()
                       return
                   }
                   let msg = response?["errorMessage"].stringValue
                   print("Message: \(String(describing: msg))")
                   self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                   self.alertView.isHidden = false
                   self.stopLoading()
                   return
               }
               
               self.stopLoading()
               print(response!)
               
               self.shadowView.isHidden = true
               self.incomeView.setView(view: self.incomeView, hidden: true)
               
               if let message = response!["data"]["successMessage"].string {
                   self.successView.alertShow(title: "Great Job!", msg: message)
                   self.successView.isHidden = false
               }
               
           })
           
       }
      
    
    
}

extension ExpenseManagerViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.incomeTableView {
            return self.incomeListArray.count
        }
        else {
            return self.expenseListArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(24)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ExpenseCell = tableView.dequeueReusableCell(withIdentifier:String(describing:ExpenseCell.self)) as!  ExpenseCell
        
        if tableView == self.incomeTableView {
            
            let incomeObj = self.incomeListArray[indexPath.row]
            
            if let title = incomeObj.name {
                cell.lblTitle.text = title
            }
            
            if let amount = incomeObj.amount {
                cell.lblAmount.text = "\(amount.decimal)"
            }
            
            return cell
            
        }
        else {
            
            let expenseObj = self.expenseListArray[indexPath.row]
            
            if let title = expenseObj.name {
                cell.lblTitle.text = title
            }
            
            if let amount = expenseObj.amount {
                cell.lblAmount.text = "\(amount.decimal)"
            }
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.incomeTableView {
            
            let incomeObj = self.incomeListArray[indexPath.row]
            
            self.type = "Edit Income"
            self.lblTitle.text = "Edit Income"
            self.deleteBtn.isHidden = false
            
            if let title = incomeObj.name {
                self.txtTitle.text = title
            }
            
            if let amount = incomeObj.amount {
                self.txtAmount.text = "\(amount)"
            }
            
            if let id = incomeObj.id {
                self.typeId = id
            }
            
            self.shadowView.isHidden = false
            self.incomeView.setView(view: incomeView, hidden: false)
            
        }
        else {
            
            let expenseObj = self.expenseListArray[indexPath.row]
            
            self.type = "Edit Expense"
            self.lblTitle.text = "Edit Expense"
            self.deleteBtn.isHidden =  false
            
            if let title = expenseObj.name {
                self.txtTitle.text = title
            }
            
            if let amount = expenseObj.amount {
                self.txtAmount.text = "\(amount)"
            }
            
            if let id = expenseObj.id {
                self.typeId = id
            }
            
            self.shadowView.isHidden = false
            self.incomeView.setView(view: incomeView, hidden: false)
        }
    }
    
}

extension ExpenseManagerViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: EventsViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = EventsViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: WellBeingViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = WellBeingViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: HomeOffersViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = HomeOffersViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                    break
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ConnectViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
                else {
                    let vc = ConnectViewController.instantiateFromStoryboard()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
        }
    }
}
