//
//  VerifyAccountViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 05/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ALCameraViewController
import AWSS3
import AWSCore

extension VerifyAccountViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

class VerifyAccountViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> VerifyAccountViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! VerifyAccountViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.title = "Verify Account"
        self.navigationController?.isNavigationBarHidden = false
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.lblOne.text = "Help us protect our community"
        self.lblTwo.text = "Please verify that you are a teacher..."
        self.lblThree.text = "Please upload a photo of your,\nTeacher ID or work visa."
        
        self.LightStatusBar()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// Image
extension VerifyAccountViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func imageAction(_ sender:UIButton) {
        self.view.endEditing(true)
        
        let check = UserDefaults.standard.object(forKey: VERIFIED) as? String
        
        if check == "false" {
            self.openCamera()
        }
        else {
            
            self.navigationController?.isNavigationBarHidden = true
            
            let msg = "Your Account is\nalready Verified!"
            self.successView.alertShow(title: "Excellent!", msg: msg)
            self.successView.isHidden = false
        }
        
    }
    
    func openCamera(){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            
            if let pickedImage = image {
                
                self?.dismiss(animated: true, completion: nil)
                self?.uploadImageAWS(withImage: pickedImage)
                
            }
            
            self?.dismiss(animated: true, completion: nil)
        
        }
        
        present(cameraViewController, animated: true, completion: nil)
        
    }
    
    func uploadImageAWS(withImage image: UIImage) {
        
        self.startLoading(message: "")
        var imageURL:String = ""
        
        let credentials = AWSStaticCredentialsProvider(accessKey: AWS_ACCESS_KEY , secretKey: AWS_SECRET_KEY)
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSouth1 , credentialsProvider: credentials)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let s3BucketName = AWS_BUCKET_NAME
        let data: Data = image.jpegData(compressionQuality: 0.5)!
        let remoteName = generateRandomStringWithLength(length: 10) + "." + data.format
        print("REMOTE NAME : ",remoteName)
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = { (task, progress) in
            DispatchQueue.main.async(execute: {
                // Update a progress bar
                print("Task: \(task)")
                print("Progress: \(progress)")
                
            })
        }
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                
                if let error = error as? String {
                    self.alertView.alertShow(image: FAILURE_IMAGE , title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                
                self.stopLoading()
                self.uploadDocumentImage(imageName: imageURL)
                
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data, bucket: s3BucketName, key: remoteName, contentType: "image/" + data.format, expression: expression, completionHandler: completionHandler).continueWith { (task) -> Any? in
            
            if let error = task.error as? String {
                self.alertView.alertShow(image: FAILURE_IMAGE , title: "Error", msg: error , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
            }
            else {
                
                if task.result != nil {
                    
                    let url = AWSS3.default().configuration.endpoint.url
                    print("url: \(url!)")
                    
                    let publicURL =  url?.appendingPathComponent(AWS_BUCKET_NAME).appendingPathComponent(remoteName)
                    
                    if let absoluteString = publicURL?.absoluteString {
                        print("Image URL : ",absoluteString)
                        imageURL = absoluteString
                        
                    }
                }
            }
            
            return nil
            
        }
        
    }
    
    func uploadDocumentImage(imageName:String) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["teacherId":id , "emiratesIdUrl":imageName ]
        print(param)
        
        ProfileServices.VerifyAccount(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.navigationController?.isNavigationBarHidden = true
            UserDefaults.standard.set("true", forKey: SUBMITTED)
            
            let msg = "Your Account\nhas been submitted\nfor verification"
            self.successView.alertShow(title: "Great Job!", msg: msg)
            self.successView.isHidden = false
            
        })
        
    }
}
