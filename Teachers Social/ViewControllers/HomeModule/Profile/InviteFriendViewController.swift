//
//  InviteFriendViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 06/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension InviteFriendViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

class InviteFriendViewController: UIViewController {

    class func instantiateFromStoryboard() -> InviteFriendViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InviteFriendViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    @IBOutlet weak var txtEmail:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.title = "Invite Friend"
        self.navigationController?.isNavigationBarHidden = false
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.lblOne.text = "SHARING IS CARING"
        self.lblTwo.text = "Help us spread the word.."
        self.lblThree.text = "Invite your teacher friends to the\nTeacher Socials community"
        
        self.LightStatusBar()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func termsAction(_ sender : UIButton) {
        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "will be implemented" , id: 0)
        self.alertView.isHidden = false
        return
    }
    
    @IBAction func SendAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Alert", subTitle: "Please enter valid email" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let param:[String:Any] = ["name":"" , "friendEmailId":email]
        print(param)

        ProfileServices.InviteFriend(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.navigationController?.isNavigationBarHidden = true
            
            let msg = "You have invited your friend to\nTeacher Socials"
            self.successView.alertShow(title: "How thoughtful!", msg: msg)
            self.successView.isHidden = false
            
        })
        
    }

}
