//
//  SubscribedViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 27/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SubscribedViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class SubscribedViewController: UIViewController {

    class func instantiateFromStoryboard() -> SubscribedViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SubscribedViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String = ""
    
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Get Subscription text
        self.GetSubscriptionText()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.lblOne.text = "Enjoy Your Best Offers"
        self.lblTwo.text = ""
//        self.lblTwo.text = "You have 365\ndays of unlimited\noffers"
        
        self.title = "My Subscription"
        self.navigationController?.isNavigationBarHidden = false
        self.PlaceLeftButton(image: BACK_IMAGE, selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.LightStatusBar()
    
    }

    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func letsGoAction(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeOffersViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func GetSubscriptionText() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id
        
        let param:[String:Any] = ["teacherId":id!]
        print(param)
        
        ProfileServices.GetSubscriptionMessage(param: param , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                
                self.lblTwo.text = ""
                
                return
            }
            
            if let subscriptionMessage = response?["data"]["successMessage"].stringValue {
                self.lblTwo.text = subscriptionMessage
            }
            
            self.stopLoading()
            print(response!)
            
        })
        
    }
}
