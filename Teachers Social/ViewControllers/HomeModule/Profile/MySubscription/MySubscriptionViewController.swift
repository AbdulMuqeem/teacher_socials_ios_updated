//
//  MySubscriptionViewController.swift
//  Teachers Social
//
//  Created by Abdul Muqeem on 27/11/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension MySubscriptionViewController : AlertViewDelegate , SuccessViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    func doneAction() {
        self.delegate?.StartRefresh()
        self.dismiss(animated: true, completion: nil)
    }
    
}

class MySubscriptionViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> MySubscriptionViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MySubscriptionViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var successView:SuccessView!
    
    var textAlert:String = ""
    var delegate:RefreshDelegate?
    
    @IBOutlet weak var tabBar:UITabBar!
    @IBOutlet weak var lblOne:UILabel!
    @IBOutlet weak var lblTwo:UILabel!
    @IBOutlet weak var lblThree:UILabel!
    
    @IBOutlet weak var smartPassView:UIView!
    @IBOutlet weak var txtSmartPass:UITextField!
    
    let tabBarImagesArray = ["events_icons" , "favourite_icon" , "offers_icon" , "connect_icon" , "profile_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.successView.delegate = self
        self.successView.isHidden = true
        
        self.txtSmartPass.delegate = self
        
        self.TabBarInit()
        self.tabBar.delegate = self
        
        self.smartPassView.isHidden = true
        
        self.lblOne.text = "To unlock all of the amazing offers and connect with \nthe Largest Teaching Community in the\nregion!"
        self.lblOne.font = UIFont(name: "HelveticaNeue", size: CGFloat(18))
        self.lblTwo.text = "Only 99 AED per year!"
        self.lblTwo.font = UIFont(name: "HelveticaNeue-Medium", size: CGFloat(25))
        self.lblThree.text = "Grab a bargain and be a part of the\nTeacher Social Community\n\nHappy Teachers, Teach Happy!"
        self.lblThree.font = UIFont(name: "HelveticaNeue", size: CGFloat(18))
        
    }
    
    @IBAction func startAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.smartPassView.setView(view: smartPassView, hidden: false)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        self.smartPassView.setView(view: smartPassView, hidden: true)
        
    }
    
    @IBAction func subscriptionAction(_ sender : UIButton) {
        self.view.endEditing(true)
        
        let smartPass = self.txtSmartPass.text!
        
        if smartPass.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter smart pass" , style: .warning)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["teacherId": id , "smartPass" : smartPass ]
        print(params)
        
        ProfileServices.GetSubscription(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.smartPassView.setView(view: self.smartPassView, hidden: true)
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["errorMessage"].stringValue
                print("Message: \(String(describing: msg))")
                self.smartPassView.setView(view: self.smartPassView, hidden: true)
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.smartPassView.setView(view: self.smartPassView, hidden: true)
            UserDefaults.standard.set("true", forKey: SUBSCRIPTION)
            
            if let message = response!["data"]["successMessage"].string {
                self.successView.alertShow(title: "Great Job!", msg: message)
                self.successView.isHidden = false
            }
            
        })
        
        
    }
    
}

extension MySubscriptionViewController : UITabBarDelegate {
    
    func TabBarInit() {
        
        self.tabBar.selectedItem = self.tabBar.items![4] as UITabBarItem
        self.tabBar.barTintColor = UIColor.init(rgb: THEME_COLOR)
        
        // Text
        let tabBarUnSelectedTitleColor   = UIColor.white
        let tabBarSelectedTitleColor   = UIColor.init(rgb: THEME_COLOR)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarUnSelectedTitleColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: tabBarSelectedTitleColor], for: .selected)
        
        // Image
        if let count = self.tabBar.items?.count {
            
            for i in 0...(count-1) {
                
                let imageNameForSelectedState   = tabBarImagesArray[i]
                
                if #available(iOS 13.0, *) {
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withTintColor(UIColor.init(rgb: THEME_COLOR))
                } else {
                    self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                }
                
                self.tabBar.items?[i].image = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        let itemIndex = 4
        let bgColor = UIColor.white
        let itemWidth = UIScreen.main.bounds.width / 5
        let bgView = UIView(frame: CGRect(x: itemWidth * CGFloat(itemIndex), y: 0, width: itemWidth, height: tabBar.frame.height))
        bgView.backgroundColor = bgColor
        tabBar.insertSubview(bgView, at: itemIndex)
        
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if self.tabBar.selectedItem == self.tabBar.items![0] as UITabBarItem {
            self.delegate?.StartRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![1] as UITabBarItem {
            self.delegate?.StartRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![2] as UITabBarItem {
            self.delegate?.StartRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![3] as UITabBarItem {
            self.delegate?.StartRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        else if self.tabBar.selectedItem == self.tabBar.items![4] as UITabBarItem {
            self.delegate?.StartRefresh()
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension MySubscriptionViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let char = string.cString(using: String.Encoding.utf8)
        let isBackSpace = strcmp(char, "\\b")
        if isBackSpace == -92 {
            return true
        }
        
        return textField.text!.count <= 3
        
    }
    
}
